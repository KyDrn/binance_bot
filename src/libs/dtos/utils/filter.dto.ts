import { IsBoolean, IsIn, IsNumber, IsOptional, IsPositive, IsString, Max, Min } from 'class-validator'

export class FilterDto {
  @IsString()
  @IsOptional()
  public search?: string

  @IsBoolean()
  @IsOptional()
  public disablePagination?: boolean

  @IsNumber()
  @IsPositive()
  @IsOptional()
  @Min(1)
  public page?: number

  @IsNumber()
  @IsPositive()
  @IsOptional()
  @Min(10)
  @Max(100)
  public itemsPerPage?: number

  @IsString()
  @IsIn([ 'ASC', 'DESC' ])
  @IsOptional()
  public sort?: 'ASC' | 'DESC'

  @IsString({ each: true })
  @IsOptional()
  public orderBy?: string[]
}

export const filterDtoSchema = {
  schema: {
    queryString: {
      type: 'object',
      properties: {
        search: { type: 'string' },
        disablePagination: { type: 'boolean' },
        page: { type: 'number' },
        itemsPerPage: { type: 'number' },
        sort: { type: 'string' },
        orderBy: { type: 'string' },
      },
      required: [],
    },
  },
}