import { get } from 'env-var'

export const getDatabaseConfig = () => ({
  host: get('DB_HOST').required().asString(),
  database: get('DB_DATABASE').required().asString(),
  username: get('DB_USER').required().asString(),
  password: get('DB_PASSWORD').required().asString(),
})