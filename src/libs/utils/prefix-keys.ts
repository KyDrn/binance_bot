export type PrefixKeys<Type, Prefix extends string, Separator extends string = ''> = {
  [Key in string & keyof Type as `${Prefix}${Separator}${Key}`]: Type[Key]
}