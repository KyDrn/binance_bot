import camelcase from 'camelcase'
import type { ClassConstructor } from 'class-transformer'
import type { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'
import type { BaseEntity } from '../../../database/entities'
import { EntityStillLinkedError } from '../errors/src/errors/entity-still-linked-error'
import type { StringKeys } from '../string-keys'
import { QueryFailedError, getRepository } from 'typeorm'
import type { Repository, SelectQueryBuilder } from 'typeorm'
import type { FilterDto } from '../../dtos'

export class CrudModel<Entity extends BaseEntity, FilterTypeDto extends FilterDto = FilterDto> {
  public readonly entity: ClassConstructor<Entity>

  protected readonly repository: Repository<Entity>

  protected readonly relations: string[]

  protected readonly searchBy: string[]

  protected readonly orderBy: string[]

  public constructor(
    entity: ClassConstructor<Entity>,
    relations: StringKeys<Entity>[] = [],
    searchBy: StringKeys<Entity>[] = [],
    orderBy: StringKeys<Entity>[] = []
  ) {
    this.entity = entity
    this.repository = getRepository(entity)
    this.relations = relations
    this.searchBy = searchBy
    this.orderBy = orderBy
  }

  public async list(filter?: FilterTypeDto): Promise<[Entity[], number]> {
    let query = this.repository.createQueryBuilder('entity')
    query = this.loadRelations(query)

    query = this.searchElements(query, filter?.search ?? '')
    query = this.paginate(query, filter?.disablePagination ?? false, filter?.itemsPerPage ?? 20, filter?.page ?? 1)
    query = this.order(query, filter?.orderBy ?? this.orderBy, filter?.sort ?? 'ASC')


    return query.getManyAndCount()
  }

  public async get(id: string): Promise<Entity> {
    let query = this.repository.createQueryBuilder('entity')
    query = this.loadRelations(query)
    query = query.where('entity.id = :id', { id })

    return query.getOneOrFail()
  }

  public async create(item: Entity): Promise<Entity> {
    const result = await this.repository.insert(item as unknown as QueryDeepPartialEntity<Entity>)
    return this.get(result.identifiers[0] ? result.identifiers[0]['id'] : '')
  }

  public async update(id: string, itemUpdated: Partial<Entity>): Promise<Entity> {
    await this.repository.update(id, itemUpdated as unknown as QueryDeepPartialEntity<Entity>)
    return this.get(id)
  }

  public async destroy(id: string): Promise<void> {
    const item = await this.repository.findOneOrFail(id)

    try {
      await this.repository.remove(item)
    } catch (error: unknown) {
      throw error instanceof QueryFailedError &&
      error.message.includes('violates foreign key constraint')
        ? new EntityStillLinkedError()
        : error
    }
  }

  protected loadRelations<T>(query: SelectQueryBuilder<T>): SelectQueryBuilder<T> {
    let modifiedQuery = query

    this.relations.forEach((relation) => {
      const [ selectedRelation, alias ] = this.parsePropertyPath(relation)

      modifiedQuery = modifiedQuery.leftJoinAndSelect(selectedRelation, alias)
    })

    return modifiedQuery
  }

  protected searchElements<T>(query: SelectQueryBuilder<T>, search?: string | null): SelectQueryBuilder<T> {
    let modifiedQuery = query

    if (search != undefined) {
      this.searchBy.forEach((element, index) => {
        const [relation] = this.parsePropertyPath(element)

        modifiedQuery = index === 0 
          ? modifiedQuery.where(`${relation} ilike :search`, { search: `%${search}%` }) 
          : modifiedQuery.orWhere(`${relation} ilike :search`, { search: `%${search}%` })
      })
    }

    return modifiedQuery
  }

  protected paginate<T>(query: SelectQueryBuilder<T>, disablePagination: boolean, itemsPerPage: number, page: number): SelectQueryBuilder<T> {
    const skip = disablePagination ? undefined : itemsPerPage * (page - 1)
    const take = disablePagination ? undefined : itemsPerPage

    return query.skip(skip).take(take)
  }

  protected order<T>(query: SelectQueryBuilder<T>, orderBy: string[], sort: 'ASC' | 'DESC'): SelectQueryBuilder<T> {
    let modifiedQuery = query

    orderBy.forEach((element, index) => {
      const [relation] = this.parsePropertyPath(element)

      modifiedQuery = index === 0
        ? modifiedQuery.orderBy(relation, sort, 'NULLS LAST')
        : modifiedQuery.addOrderBy(relation, sort, 'NULLS LAST')
    })

    return modifiedQuery
  }

  protected parsePropertyPath(path: string): [string, string] {
    const alias = camelcase(path)
    let ormPath: [string, string]

    // eslint-disable-next-line prefer-const
    let [ parentRelation, relation ] = path.split('.').slice(-2)

    if (relation === undefined) {
      ormPath = [ `entity.${parentRelation}`, alias ]
    } else {
      const parentAliasArray = path.split('.')
      parentAliasArray.pop()
      const parentAlias = camelcase(parentAliasArray.join('.'))

      ormPath = [ `${parentAlias}.${relation}`, alias ]
    }

    return ormPath
  }
}