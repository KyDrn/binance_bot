import type { BaseEntity } from "../../../database/entities";
import { BaseDto, FilterDto, Id, ListResult } from "../../dtos";
import { validateJson } from "../validate-json";
import type { CrudModel } from "./crud.model";

export class CrudController<
Dto extends BaseDto & CreateDto,
CreateDto,
UpdateDto extends Partial<CreateDto>,
Entity extends BaseEntity,
Model extends CrudModel<Entity, FilterTypeDto>,
FilterTypeDto extends FilterDto = FilterDto
> {
  protected readonly model: Model
  protected readonly dto: Dto
  protected readonly createDto: CreateDto
  protected readonly updateDto: UpdateDto
  protected readonly filterDto: FilterTypeDto

  public constructor(model: Model, dtos: {
    base: Dto
    create: CreateDto
    update: UpdateDto
  }, filterDto: FilterTypeDto) {
    this.model = model
    this.dto = dtos.base
    this.createDto = dtos.create
    this.updateDto = dtos.update
    this.filterDto = filterDto
  }

  public async list(): Promise<ListResult<Dto>> {
    const filter = await validateJson(this.filterDto)
    const [result, total] = await this.model.list(filter)
    const mappedResult = await Promise.all(
      result.map(async (item) => this.dtoMapper(item))
    )

    return {
      data: mappedResult,
      total
    }
  }

  public async get(): Promise<Dto> {
    const key = await validateJson(Id as unknown as Id)
    const result = await this.model.get(key.id)

    return this.dtoMapper(result)
  }

  public async create(): Promise<Dto> {
    const item = await validateJson(this.createDto)
    const mappedEntity = await this.createEntityMapper(item)
    const result = await this.model.create(mappedEntity)

    return this.dtoMapper(result)
  }

  public async update(): Promise<Dto> {
    const key = await validateJson(Id as unknown as Id)
    const item = await validateJson(this.updateDto)
    const mappedEntity = await this.updateEntityMapper(item)
    const result = await this.model.update(key.id, mappedEntity)

    return this.dtoMapper(result)
  }

  public async destroy(): Promise<void> {
    const key = await validateJson(Id as unknown as Id)
    await this.model.destroy(key.id)
  }

  public async dtoMapper(entity: Entity): Promise<Dto> {
    return entity as unknown as Dto
  }

  public async entityMapper(dto: Dto): Promise<Entity> {
    return dto as unknown as Entity
  }

  public async createEntityMapper(dto: CreateDto): Promise<Entity> {
    return dto as unknown as Entity
  }

  public async updateEntityMapper(dto: UpdateDto): Promise<Entity> {
    return dto as unknown as Entity
  }
}