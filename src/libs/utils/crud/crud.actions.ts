import type { BaseDto, FilterDto, Id } from '../../dtos'

interface Actions<
  Dto extends BaseDto & CreateDto,
  CreateDto,
  UpdateDto extends Partial<CreateDto>
> {
  list: {
    params: FilterDto | undefined
    returns: Dto
  }
  get: {
    params: Id
    return: Dto
  }
  create: {
    params: CreateDto
    return: Dto
  }
  update: {
    params: Id & UpdateDto
    return: Dto
  }
  delete: {
    params: Id
    return: Dto
  }
  destroy: {
    params: Id
    return: void
  }
}

export type CrudActions<
  Dto extends BaseDto & CreateDto,
  CreateDto,
  UpdateDto extends Partial<CreateDto>
> = Actions<Dto, CreateDto, UpdateDto>