import type { Id } from '../../dtos/utils'

interface Events<Dto extends Id> {
  created: {
    payload: Pick<Dto, 'id'>
  }
  updated: {
    payload: Pick<Dto, 'id'>
  }
  deleted: {
    payload: Pick<Dto, 'id'>
  }
  destroyed: {
    payload: {
      data: Record<string, unknown>
    }
  }
}

export type CrudEvents<Dto extends Id> = Events<Dto>