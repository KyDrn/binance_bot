import { validate } from 'class-validator'
import { ValidationError } from './errors/src/errors/validation-error'

export const validateJson = async <T>(clazz: T): Promise<T> => {

  const errors = await validate(clazz as unknown as object)
  if (errors.length > 0) {
    throw new ValidationError(errors.flatMap((error) => Object.values(error.constraints ?? {})))
  }
  return clazz
}