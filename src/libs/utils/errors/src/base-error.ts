import type { HttpErrorCodes } from "fastify-sensible/lib/httpError"



export abstract class BaseError extends Error {
  public code: Extract<HttpErrorCodes, number>

  protected constructor(name: string, message: string, statusCode: Extract<HttpErrorCodes, number>) {
    super()
    this.name = name
    this.message = message
    this.code = statusCode
  }
}