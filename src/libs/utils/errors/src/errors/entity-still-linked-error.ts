import { snakeCase } from "snake-case";
import { BaseError } from "../base-error";

export class EntityStillLinkedError extends BaseError {
  public constructor() {
    super(
      snakeCase(EntityStillLinkedError.name).toUpperCase(),
      "This entity cannot be deleted because it's still linked.",
      400,
    )
  }
}