import { snakeCase } from "snake-case";
import { BaseError } from "../base-error";

export class ValidationError extends BaseError {
  public readonly messages: string[]

  public constructor(messages: string[]) {
    super(snakeCase(ValidationError.name).toUpperCase(), 'Unprocessable entity', 422)

    this.messages = messages
  }
}