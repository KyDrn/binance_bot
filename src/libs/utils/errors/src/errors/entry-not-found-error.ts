import { BaseError } from '../base-error'
import { snakeCase } from 'snake-case'

export class EntryNotFoundError extends BaseError {
  public constructor() {
    super(
      snakeCase(EntryNotFoundError.name).toUpperCase(),
      'The entry was not found in the directory',
      404,
    )
  }
}