import type { FastifyInstance } from 'fastify'
import type { HttpErrorCodes } from 'fastify-sensible/lib/httpError'
import * as Errors from './errors'

type ErrorHandler = Parameters<FastifyInstance['setErrorHandler']>['0']

type ErrorMatcher = Record<string, Extract<HttpErrorCodes, number>>

const handledErrors = Object.entries(Errors)

let httpCode: Extract<HttpErrorCodes, number> = 500
let found = false

const handleKnownErrors = (error: Error): void => {
  const handledError = handledErrors.find(([key]) => key === error.name)

  if (handledError !== undefined) {
    found = true
    httpCode = new handledError[1]().code
  }
}

const handleMatchedErrors = (matcher: ErrorMatcher, error: Error): void => {
  if (found) {
    return
  }

  const normalized = error.name.endsWith('Error')
    ? error.name.slice(0, -'Error'.length)
    : error.name

  httpCode = matcher[normalized] ?? matcher[`${normalized}Error`] ?? 500
}

export const createErrorHandler = (matcher: ErrorMatcher, broker: FastifyInstance ): ErrorHandler => (error, _request, response) => {
  const { name, message } = error

  handleKnownErrors(error)
  handleMatchedErrors(matcher, error)

  if (httpCode === 500) {
    broker.log.error(`${name}: ${message}`)
  } else {
    broker.log.debug(`\n\n ${name}: ${message} \n`)
  }

  response.status(httpCode)
}