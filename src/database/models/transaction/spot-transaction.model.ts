import { SpotTransactionEntity } from '../../entities'
import { Timestamp, getConnection } from 'typeorm'

export class SpotTransactionController {

    getAll = async () => {
      return await getConnection().manager.find(SpotTransactionEntity)
    }

    getByAccount = async (accountId: string) => {
      return await getConnection().manager.createQueryBuilder(SpotTransactionEntity, 'transaction')
        .leftJoinAndSelect('transaction.account', 'account', 'account.id = :accountId', { accountId: accountId })
        .getMany()
    }

    getByDateRange = async (accountId: string, start: Timestamp, end: Timestamp) => {
      return await getConnection().manager.createQueryBuilder(SpotTransactionEntity, 'transaction')
        .leftJoinAndSelect('transaction.account', 'account', 'account.id = :accountId', { accountId: accountId })
        .where('transaction.date >= :start', { start: start })
        .andWhere('transacation.date < :end', { end: end })
        .getMany()
    }

    getByPair = async (accountId: string, pair: string) => {
      return await getConnection().manager.createQueryBuilder(SpotTransactionEntity, 'transaction')
        .leftJoinAndSelect('transaction.account', 'account', 'account.id = :accountId', { accountId: accountId })
        .where('transaction.pair = :pair', { pair: pair })
        .getMany()
    }
}