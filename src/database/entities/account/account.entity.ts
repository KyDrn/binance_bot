import { BalanceHistoryEntity } from './balance-history.entity'
import { FuturesTransactionEntity } from '../transaction/futures-transaction.entity'
import { SpotTransactionEntity } from '../transaction/spot-transaction.entity'
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'

@Entity('Accounts')
export class AccountEntity {

    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name!: string

    @Column()
    balance!: number;

    @Column()
    exchange!: string;
    
    @Column()
    rawResult?: number;
    
    @Column()
    percentageResult?: number;
    
    @OneToMany(() => SpotTransactionEntity, (item) => item.account)
    spotTransactions!: SpotTransactionEntity[];

    @OneToMany(() => FuturesTransactionEntity, (item) => item.account)
    futuresTransactions!: FuturesTransactionEntity[];

    @OneToMany(() => BalanceHistoryEntity, (item) => item.account)
    balanceHistories!: BalanceHistoryEntity[];
}