import { AccountEntity } from './account.entity'
import { BaseEntity } from '../base.entity'
import { Column, Entity, OneToMany, Timestamp } from 'typeorm'

@Entity('BalanceHistory')
export class BalanceHistoryEntity extends BaseEntity {

    @Column()
    value!: number

    @Column()
    date!: Timestamp;

    @Column()
    pair!: string;

    @Column()
    amount!: number;

    @Column()
    rawResult?: number;

    @Column()
    percentageResult?: number;

    @OneToMany(() => AccountEntity, (item) => item.balanceHistories)
    account!: AccountEntity;

}