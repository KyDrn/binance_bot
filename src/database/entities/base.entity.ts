import { Column } from "typeorm";

export class BaseEntity {
    @Column({
        type: 'uuid',
        generated: 'uuid',
        primary: true,
    })
    public id!: string
}