import { AccountEntity } from '../account/account.entity'
import { BaseEntity } from '../base.entity'
import { Column, Entity, ManyToOne, Timestamp } from 'typeorm'

@Entity('SpotTransactions')
export class SpotTransactionEntity extends BaseEntity {

    @Column()
    status!: 'open' | 'closed'

    @Column()
    date!: Timestamp;

    @Column()
    pair!: string;

    @Column()
    amount!: number;

    @Column()
    public rawResult?: number;

    @Column()
    public percentageResult?: number;

    @ManyToOne(() => AccountEntity, (item) => item.spotTransactions)
    public account!: AccountEntity

}