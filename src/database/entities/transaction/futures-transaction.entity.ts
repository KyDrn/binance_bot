import { AccountEntity } from '../account/account.entity'
import { BaseEntity } from '../base.entity'
import { Column, Entity, ManyToOne, Timestamp } from 'typeorm'

@Entity('FuturesTransactions')
export class FuturesTransactionEntity extends BaseEntity {

    @Column()
    status!: 'open' | 'closed'

    @Column()
    type!: 'long' | 'short'

    @Column()
    date!: Timestamp;

    @Column()
    pair!: string;

    @Column()
    amount!: number;

    @Column()
    leverage!: number

    @Column()
    rawResult?: number;

    @Column()
    percentageResult?: number;

    @ManyToOne(() => AccountEntity, (item) => item.futuresTransactions)
    public account!: AccountEntity

}