import * as entities from '../entities'
import { createConnection } from 'typeorm'
import { getDatabaseConfig } from '../../libs/env-config'


const main = async (): Promise<void> => {
  const databaseConfig = getDatabaseConfig()
  try {
    const connection = await createConnection({
      ...databaseConfig,
      type: 'postgres',
      entities: Object.values(entities),
    })
    await connection.synchronize()
  } catch (error: unknown) {
    console.error('Database connection failed : ', error)
  }
}

void main()