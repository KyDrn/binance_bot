import express from 'express';
import Binance from 'binance-api-node'
import { runBot } from './telegram-bot'


const app = express();
const port = 3000;

const client = Binance()
runBot()


app.get('/', async (req, res) => {
    res.send('Hello World');
    //console.log(await (await client.book({ symbol: 'ETHBTC' })))
});

app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
})