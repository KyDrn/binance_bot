import dotenv from 'dotenv'
import { Telegraf } from 'telegraf'


export const runBot = () => {
  dotenv.config()
  console.log(process.env['TELEGRAM_BOT_KEY'])
  const bot = new Telegraf(process.env['TELEGRAM_BOT_KEY'] ?? '')

  bot.start((ctx) => ctx.reply('STARTING'))

  bot.on('message', (ctx) => ctx.telegram.sendMessage(ctx.chat.id, 'Hello moi'))

  bot.launch()
}