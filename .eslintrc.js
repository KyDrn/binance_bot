module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
    'node': true,
  },
  'extends': [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module'
  },
  'plugins': [
    '@typescript-eslint',
    'eslint-plugin-import-order-alphabetical'
  ],
  'rules': {
    'quotes': [ 'error', 'single' ],
    'semi': [ 'error', 'never' ],
    'indent': [ 'error', 2 ],
    'no-multi-spaces': ['error'],
    'object-curly-spacing': [ 'error', 'always' ],
    'array-bracket-spacing': [ 'error', 'always', { 'singleValue': false } ],
    'sort-imports': [ 2, { 'memberSyntaxSortOrder': [ 'none', 'all', 'single', 'multiple' ]
    } ],
    'import-order-alphabetical/order': 'error',
    
  }
}
