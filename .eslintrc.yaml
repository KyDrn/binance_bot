parser: '@typescript-eslint/parser'
parserOptions:
  ecmaVersion: 2021
  project: tsconfig.json
  sourceType: module

settings:
  import/resolver:
    typescript:

extends:
  - plugin:@typescript-eslint/recommended
  - plugin:prettier/recommended

plugins:
  - simple-import-sort
  - import
  - only-error
  - unused-imports

rules:
  default-case: error
  no-async-promise-executor: error
  no-await-in-loop: error
  no-console: error
  no-misleading-character-class: error
  no-multi-assign: error
  no-multi-str: error
  no-nested-ternary: error
  no-new: error
  no-new-object: error
  no-new-symbol: error
  no-new-wrappers: error
  no-obj-calls: error
  no-path-concat: error
  no-return-await: error
  no-script-url: error
  no-self-compare: error
  no-sequences: error
  no-shadow-restricted-names: error
  no-sparse-arrays: error
  no-tabs: error
  no-template-curly-in-string: error
  no-this-before-super: error
  prefer-numeric-literals: error
  prefer-object-spread: error
  prefer-rest-params: error
  prefer-spread: error
  prefer-template: error
  sort-imports: off
  symbol-description: error

  import/exports-last: error
  import/first: error
  import/no-default-export: error
  import/no-duplicates: error
  import/order: off
  import/prefer-default-export: off

  simple-import-sort/exports: error
  simple-import-sort/imports: error

  unused-imports/no-unused-imports: error

  '@typescript-estlint/member-ordering': error
  '@typescript-estlint/method-signature-style': error
  '@typescript-estlint/naming-convention': error
  '@typescript-estlint/no-base-to-string': error
  '@typescript-estlint/no-confusing-non-null-assertion': error
  '@typescript-estlint/no-confusing-void-expression': error
  '@typescript-estlint/no-dynamic-delete': error
  '@typescript-estlint/no-floating-promises': error
  '@typescript-estlint/no-implicit-any-catch': error
  '@typescript-estlint/no-require-imports': error
  '@typescript-estlint/no-unnecessary-boolean-literal-compare': error
  '@typescript-estlint/no-unnecessary-condition': error
  '@typescript-estlint/no-unnecessary-qualifier': error
  '@typescript-estlint/no-unnecessary-type-arguments': error
  '@typescript-estlint/no-unnecessary-type-constraint': error
  '@typescript-estlint/no-unused-vars': off
  '@typescript-estlint/prefer-includes': error
  '@typescript-estlint/prefer-nullish-coalescing': error
  '@typescript-estlint/prefer-optional-chain': error
  '@typescript-estlint/prefer-readonly': error
  '@typescript-estlint/prefer-string-starts-ends-with': error
  '@typescript-estlint/prefer-ts-expect-error': error
  '@typescript-estlint/promise-function-async': error
  '@typescript-estlint/require-array-sort-compare': error
  '@typescript-estlint/sort-type-union-intersection-members': error
  '@typescript-estlint/strict-boolean-expressions': error
  '@typescript-estlint/switch-exhaustiveness-check': error
  '@typescript-estlint/type-annotation-spacing': error
  '@typescript-estlint/unified-signatures': error

  arrow-body-style:
    - error
    - as-needed

  func-style:
    - error
    - expression
    - allowArrowFunctions: true

  prefer-arrow-callback:
    - error
    - allowNamedFunctions: true

  import/newline-after-import:
    - error
    - count: 1

  unused-imports/no-unused-vars:
    - error
    - vars: all
      args: after-used
      ignoreRestSiblings: true
      argsIgnorePattern: ^_
      varsIgnorePattern: ^_

  '@typescript-eslint/array-type':
    - error
    - default: array
      readonly: array

  '@typescript-eslint/consistent-indexed-object-style':
    - error
    - record

  '@typescript-eslint/consistent-type-assertions':
    - error
    - assertionStyle: as
      objectLiteralTypeAssertions: allow

  '@typescript-eslint/consistent-type-definitions':
    - error
    - interface

  '@typescript-eslint/consistent-type-imports':
    - error
    - prefer: type-imports
      disallowTypeAnnotations: true
  
  '@typescript-eslint/explicit-function-return-type':
    - error
    - allowExpressions: true
      allowTypedFunctionExpressions: true
      allowHigherOrderFunctions: true
      allowDirectConstAssertionInArrowFunctions: true
      allowConciseArrowFunctionExpressionsStartingWithVoid: false

  '@typescript-eslint/explicit-member-accessibility':
    - error
    - accessibility: explicit

  '@typescript-eslint/no-extraneous-class':
    - error
    - allowWithDecorator: true

  '@typescript-eslint/no-parameters-properties':
    - error
    - allows:
      - private readonly
      - protected readonly
      - public readonly

  'lines-between-class-members': off
  '@typescript-eslint/lines-between-class-members':
    - error
    - always
    - exceptAfterOverload: true